/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/global.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
const routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');

// or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');
require('../css/app.css');

// update ui on mercure notification
function update_job_view(job) {
    var ts = new Date(Date.parse(job.endDate) - Date.parse(job.startDate));
    var processedTime = ("0" + ts.getMinutes()).slice(-2) + ":";
    processedTime += ("0" + ts.getSeconds()).slice(-2);
    $('#job-' + job.id + ' .title-icon').html('<i class="fas fa-cog"></i>');
    $('#job-' + job.id + ' .columns').html(job.columns.length);
    $('#job-' + job.id + ' .nb-lines').html(job.nbLines);
    $('#job-' + job.id + ' .processed-in').html(processedTime + "s");
    $('#job-' + job.id + ' .to-update').each(function( index ) {
        if($( this ).css("display") == "none") {
            $( this ).css("display", "inline");
        }
    });
}

function update_alert_center(job, Routing) {
    $('#alertsCounter').html(parseInt($('#alertsCounter').html())+1);
    $('#alertsList').append(' \
    <a class="dropdown-item d-flex align-items-center" \
        href="' + Routing.generate('job_show', { id: job.id }) + '"> \
        <div class="mr-3"> \
            <div class="icon-circle bg-primary"> \
                <i class="fas fa-eye"></i> \
            </div> \
        </div> \
        <div> \
            <span class="font-weight-bold"> \
                ' + job.title + ' \
                <i class="fas fa-check-circle"></i> \
            </span> \
            <div class="small text-gray-500"> \
                ' + job.endDate + ' \
            </div> \
        </div> \
    </a> \
    ');
}

$(document).ready(function() {

    if (typeof mercure_user_topic_iri !== 'undefined') {

        const url = new URL(mercure_publish_url);
        url.searchParams.append('topic', mercure_user_topic_iri);
        const eventSource = new EventSource(url);
    
        // on mercure notification
        eventSource.onmessage = event => {
            //https://stackoverflow.com/questions/5525071/how-to-wait-until-an-element-exists/57395241#57395241
            var job = JSON.parse(event.data);
    
            if ($('#job-' + job.id).length) {
                update_job_view(job);
                update_alert_center(job, Routing);   
                return;
            }
    
            var observer = new MutationObserver(function(mutations) {
                if ($('#job-' + job.id).length) {
                    update_job_view(job);
                    update_alert_center(job, Routing);   
                    observer.disconnect(); //We can disconnect observer once the element exist if we dont want observe more changes in the DOM
                }
            });
            
            // Start observing
            observer.observe(document.body, { //document.body is node target to observe
                childList: true, //This is a must have for the observer with subtree
                subtree: true //Set to true if changes must also be observed in descendants.
            });
        }
    }
    
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    
    // disable reload page on submit new job
    // prepend returned job template
    $("#job-form").on('submit', function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        var XHR = false;
        var XHRResponse = false;
        // https://stackoverflow.com/questions/5392344/sending-multipart-formdata-with-jquery-ajax
        // https://stackoverflow.com/questions/20795449/jquery-ajax-form-submission-enctype-multipart-form-data-why-does-contentt
        formData.append('spreadsheet', $('#job_datasheet')[0].files[0]);        
        $.ajax({
            url: '/job',
            data: formData, 
            cache: false,
            contentType: false,
            processData: false,
            method: $(this).attr('method'),
            async: true,
            type: $(this).attr('method'), // For jQuery < 1.9
            global: false,
            dataType: 'html',
            success: function(data)
            {
                XHRResponse = data;
                var matcher = new RegExp("<html>", "g");
                if(matcher.test(XHRResponse)) {
                    XHR = true;
                } else {
                    $('#job_list').prepend(XHRResponse);
                    XHR = false;
                }
                return XHR;
            }
        });
    });

    // force path rendering in input file
    $('#job_datasheet').on('change',function(){
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
    })


    // order tuples
    function compareTrends(index, reverse = false) {
        return function(a, b) {            
            if(reverse) {
                return parseInt($(b).find('td.trends span.badge').length) - parseInt($(a).find('td.trends span.badge').length);
            }
            return parseInt($(a).find('td.trends span.badge').length) - parseInt($(b).find('td.trends span.badge').length);            
        }
    }
    function compareFrequency(index, reverse = false) {
        return function(a, b) { 

            if(reverse) {
                return parseInt($(b).find('td.frequency span.badge').html()) - parseInt($(a).find('td.frequency span.badge').html());
            }
            return parseInt($(a).find('td.frequency span.badge').html()) - parseInt($(b).find('td.frequency span.badge').html());
        }
    }
    $( "#trends-sort" ).click(function() {
        var rows = $("#results tr");
        rows = rows.sort(compareFrequency($(this).index(), true));
        if($(this).html().match('down')) {
            rows = rows.sort(compareTrends($(this).index(), true))
            $(this).html('<i class="fas fa-sort-up"></i>');
        }
        else {
            rows = rows.sort(compareTrends($(this).index()))
            $(this).html('<i class="fas fa-sort-down"></i>');
        }
        $("#results").html(rows);
    });
    $( "#frequency-sort" ).click(function() {
        var rows = $("#results tr");
        if($(this).html().match('down')) {
            rows = $("#results tr").sort(compareFrequency($(this).index(), true));
            $(this).html('<i class="fas fa-sort-up"></i>');
        }
        else {
            rows = $("#results tr").sort(compareFrequency($(this).index()));
            $(this).html('<i class="fas fa-sort-down"></i>');
        }
        //rows = rows.toArray().sort(compareTrends($(this).index()));
        $("#results").html(rows);
    });

    // dynamcal results render
    function refreshResult(callback = function(){}) {
        
        var columns = Array();
        $("#columns input[type='checkbox']").each(function( index ) {
            columns.push($( this ).val());
        });

        var excludedColumns = Array();
        $("#columns input[type='checkbox']").each(function( index ) {
            if(!$( this ).prop('checked')) {
                excludedColumns.push($( this ).val());
            }
        });

        //var results = Array();
        var results = 0;
        $("#results tr").each(function( table_line_index ) {

            var line = $( this ).children('td.columns').html();

            if(
                !excludedColumns.reduce(
                    function (LineHasExcludedColumns, excludedColumn, index) {

                        if(LineHasExcludedColumns) {
                            return LineHasExcludedColumns;
                        }

                        var re = new RegExp("\\s+<span class=\\\"index\\\">[\\s.]*" + excludedColumn + "[\\s.]*[\\+\\-][\\s.]*<\/span>", 'gm');
                        LineHasExcludedColumns = line.match(re);

                        return LineHasExcludedColumns;
                    }, 0
                )
            ) {
                $( this ).css('display', 'table-row');
                results++;
            } else {
                $( this ).css('display', 'none');
            }

        });
        $('#results-count').html(results);
        callback();
    }

    // to synchronize in case of check all checkbox
    $( "#check-all" ).click(function() {
        $("#columns input[type='checkbox']").each(function( index ) {
            $( this ).prop('checked',true);
        });
        refreshResult();
        return false;
    });

    // to synchronize in case of uncheck all checkbox
    $( "#uncheck-all" ).click(function() {
        $("#columns input[type='checkbox']").each(function( index ) {
            $( this ).prop('checked',false);
        });
        refreshResult();
        return false;
    });

    // to synchronize in case of toggle checkbox
    $( "#toggle-all" ).click(function() {
        $("#columns input[type='checkbox']").each(function( index ) {
            $( this ).prop('checked',!$( this ).prop('checked'));
        });
        refreshResult();
        return false;
    });

    // to synchronize in case of click on a single checkbox
    $("#columns input[type='checkbox']").click(function() {
        $('#jobModal')
            .modal('show')
            .one('shown.bs.modal', function (e) {
                refreshResult(
                    function () {
                        $('#jobModal').modal('hide');
                    }
                );
        }); 
    });

    // to synchronize in case of refresh
    refreshResult();

    // (un)fold job card
    $(".fold").click(function() {
        if($(this).parent(".card-header").next().css("display") == 'block') {
            $(this).parent(".card-header").next().css("display", "none");
            $(this).html('<i class="fas fa-plus-square"></i>');
        }
        else {
            $(this).parent(".card-header").next().css("display", "block");
            $(this).html('<i class="fas fa-minus-square"></i>');
        }
    });
});