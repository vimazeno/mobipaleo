#!/bin/bash
php bin/console doctrine:database:drop --force --no-interaction --env=test
php bin/console doctrine:database:create --no-interaction --env=test
bin/console doctrine:schema:update --force --env=test
bin/console doctrine:fixtures:load --no-interaction --env=test
php bin/phpunit $@
