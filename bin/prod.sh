php bin/console enqueue:setup-broker
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console assets:install --symlink public
php bin/console fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json
source /home/mobipaleo/.nvm/nvm.sh nvm use 12.22.7
yarn install
yarn encore production
