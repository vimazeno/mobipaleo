#!/bin/bash

command -v "vault" >/dev/null 2>&1 || {
  echo >&2 "I require vault binary to run"
  exit 1
}

if [[ -z "${VAULT_PATH}" ]] ; then
  export VAULT_PATH=cri/workspaces/projects/mobipaleo
fi
if [[ -z "${VAULT_ADDR}" ]] ; then
  export VAULT_ADDR=https://vault.cri.local.isima.fr
fi

if [[ -z "${VAULT_TOKEN}" ]] ; then
  if [[ -z "${VAULT_USERNAME}" ]] ; then
    echo uca username
    read username
    export VAULT_USERNAME=${username}
  fi
  vault login -method=ldap username=$VAULT_USERNAME > /dev/null
  export VAULT_TOKEN=$(cat ~/.vault-token)
fi

function replace_env() {

  KV=$(vault kv get -format=json ${VAULT_PATH}/${1} | python -c "import sys, json; print json.load(sys.stdin)['data']['data'].keys()")
  VAULT_KEYS=( $(echo ${KV} | sed -r "s/', u'/' '/g" | sed -r "s/\[u'/'/g" | sed -r "s/\]//g") )
  for i in "${VAULT_KEYS[@]}"
  do
    # enlève le permier '
    i=${i%\'}
    # enlève le dernier '
    i=${i#\'}
    sed -i "s|$i|$(vault kv get -format=json ${VAULT_PATH}/${1} | jq -r .data.data.$i | sed -r "s/\n//g")|g" .env 2>/dev/null
  done
  
}

if [[ -z "${APP_ENV}" ]] ; then
  cp .env.dev.sample .env
else
  cp .env.prod.sample .env
  replace_env "app"
fi
