sudo apt install php-amqp

composer install

php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create --if-not-exists
php bin/console enqueue:setup-broker
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load --no-interaction --env dev
bin/console enqueue:consume &
bin/console assets:install --symlink public
bin/console fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json
yarn install
yarn encore dev --watch &

