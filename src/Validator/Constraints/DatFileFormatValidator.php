<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

use App\Validator\Constraints\DatFileFormat;

class DatFileFormatValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof DatFileFormat) {
            throw new UnexpectedTypeException($constraint, DatFileFormat::class);
        }
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            $this->context->buildViolation($constraint->message[DatFileFormat::BLANK_FILE_PATH])
                ->addViolation();
            return;
        }

        if (!is_string($value)) {
            // throw this exception if your validator cannot handle the passed type so that it can be marked as invalid
            throw new UnexpectedValueException($value, 'string');
            // separate multiple types using pipes
            // throw new UnexpectedValueException($value, 'string|int');
        }
        
        if ($this->ValidateDatFormat($value) !== True) {
            $this->context->buildViolation($constraint->message[$this->ValidateDatFormat($value)])
                ->addViolation();
        }
    }

    private function ValidateDatFormat($filePath)
    {
        $data = file_get_contents($filePath);
        $lines = explode("\n",$data);
        foreach($lines as $k => $line)
        {
            if(trim($line) != "")
            {
                $values = preg_split("/\s+/", $line);
                // test columns consistency
                if(!$k)
                {
                    $nbColumns = count($values);
                    // test columns have no html
                    foreach($values as $value) 
                    {
                        if(strip_tags($value)!=$value)
                        {
                            return DatFileFormat::HTML_TAGS_IN_COLUMNS;
                        }
                    }
                }
                // test values consistency
                else
                {
                    // test (number of values)==(number of columns)
                    if($nbColumns!=count($values) && count($values)!=1)
                    {
                        return DatFileFormat::COLUMNS_CONSISTENCY;
                    }
                    // test values are numeric
                    foreach($values as $value) 
                    {
                        if(!is_numeric($value) && $value != "")
                        {
                            return DatFileFormat::NON_NUMERIC_VALUES;
                        }
                    }
                }
            }
            
        }
        return DatFileFormat::VALID_FILE;
    }
}