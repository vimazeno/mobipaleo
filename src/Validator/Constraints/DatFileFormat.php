<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DatFileFormat extends Constraint
{
    const BLANK_FILE_PATH      = 'blank_file_path';
    const COLUMNS_CONSISTENCY  = 'columns_consistency';
    const HTML_TAGS_IN_COLUMNS = 'html_tags_in_columns';
    const NON_NUMERIC_VALUES  = 'non_numeric_values';
    const VALID_FILE  = True;
    
    public $message = [
        self::BLANK_FILE_PATH => 'you must provide a path to a csv file',
        self::COLUMNS_CONSISTENCY => 'incorrect dat format: number of columns is inconsistent considering number of values',
        self::HTML_TAGS_IN_COLUMNS => 'incorrect dat format: you can\'t put html in column name',
        self::NON_NUMERIC_VALUES => 'incorrect dat format: you can\'t put non numeric values'
    ];
}