<?php
// src/Command/ImportLdapUserCommand.php
namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use Doctrine\ORM\EntityManagerInterface;

class ForceCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:force';
    protected $testServer = "mobipaleo.test.local.isima.fr";
    protected $prodServer = "mobipaleo.cri.local.isima.fr";
    protected $remoteSshUserKeyPath = "~/.ssh/stack/limosadm";
    protected $remoteDatasheetsDirPath = "/var/www/mobipaleo/current/public/uploads/datasheets";
    protected $remoteSshUser = "limosadm";
    protected $remoteSshPath = "/var/www/my/current";
    protected $localDbUser = "admin";

    private $em;
    private $params;

    public function __construct(
        EntityManagerInterface $entityManager,
        ParameterBagInterface $params
    )
    {
        $this->em = $entityManager;
        $this->params = $params;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addOption(
                'test',
                't',
                InputOption::VALUE_NONE
            )
            ->setDescription('Force schema /upload on prod or test.')
            ->setHelp('This command force SQL schema en upload on prod or test ...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<fg=cyan>force schema</>");
        
        $database = $this->em->getConnection()->getDatabase();
        
        if($input->getOption('test'))
            $server = $this->testServer;
        else
            $server = $this->prodServer;
        
        # import csv
        shell_exec('rm -rf '.$this->remoteDatasheetsDirPath.'/*.csv');        
        shell_exec('scp  -i '.$this->params->get('datasheets_dir').'/*.csv '.$this->remoteSshUserKeyPath.' '.$this->remoteSshUser.'@'.$server.':'.$this->remoteDatasheetsDirPath.'/');        
        # force schema
        shell_exec("mysqldump -u '.$this->localDbUser.' '.$database.' > /tmp/'.$database.'.sql");
        shell_exec('ssh -i '.$this->remoteSshUserKeyPath.' '.$this->remoteSshUser.'@'.$server.' "cd '.$this->remoteSshPath.' && php bin/console doctrine:database:drop --force"');
        shell_exec('ssh -i '.$this->remoteSshUserKeyPath.' '.$this->remoteSshUser.'@'.$server.' "cd '.$this->remoteSshPath.' && php bin/console doctrine:database:create"');
        shell_exec('scp -i '.$this->remoteSshUserKeyPath.' /tmp/'.$database.'.sql '.$this->remoteSshUser.'@'.$server.':/tmp/'.$database.'.sql');
        shell_exec('ssh -i '.$this->remoteSshUserKeyPath.' '.$this->remoteSshUser.'@'.$server.' "mysql '.$database.' < /tmp/'.$database.'.sql"');
        shell_exec('ssh -i '.$this->remoteSshUserKeyPath.' '.$this->remoteSshUser.'@'.$server.' "rm /tmp/'.$database.'.sql"');

        $output->writeln("<fg=cyan>schema forced</>");
    }
}