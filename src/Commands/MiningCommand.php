<?php

namespace App\Commands;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\Validation;

use App\Entity\Job;
use App\Services\ConstraintMining;
use App\Validator\Constraints\DatFileFormat;
use App\Validator\Constraints\DatFileFormatValidator;

class MiningCommand extends Command {

    private $constraintMining;

    public function __construct(
        ConstraintMining $constraintMining,
        ParameterBagInterface $params        
    )
    {
        parent::__construct();
        $this->constraintMining = $constraintMining;
        $this->fixturesDatasheetsDir = $params->get('fixtures_datasheets_dir');
        $this->datasheetsDir = $params->get('datasheets_dir');
    }

    protected function configure()
    {
        $this->setName("app:mining")
        ->addArgument('constraint', InputArgument::REQUIRED, 'constraint mining name ['.join('|', ConstraintMining::$constraintMiningChoices).']')
        ->addArgument('frequency', InputArgument::REQUIRED, 'frequency (should be a integer [0-100]')
        ->addArgument('file', InputArgument::REQUIRED, 'file path (can be relative from datasheets or absolute')
        ->setDescription("mine a csv file with choosen constraint mining")
        ->setHelp("mine a csv file with choosen constraint mining");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $constraint = $input->getArgument('constraint');
        $frequency = $input->getArgument('frequency');

        // compute file path (relative or absolute)
        $filePath = $input->getArgument('file');
        if(is_file($this->fixturesDatasheetsDir.'/'.$filePath)) {
            $filePath = $this->fixturesDatasheetsDir.'/'.$filePath;
        }
        
        // validate csv file
        $validator = Validation::createValidatorBuilder()
            ->addMethodMapping('ValidateDatFormat')
            ->getValidator();
        $violations = $validator->validate($filePath, new DatFileFormat());

        if(count($violations) > 0) {
            $output->writeln("<bg=red> 🗴 validation error(s)</>");
        }
        foreach($violations as $volation) 
        {
            $output->writeln(" * ".$volation->getMessage());
        }
        if(count($violations) > 0) {
            return 1;
        }
        
        // process mining constraint
        $fs = new Filesystem();
        $job = new Job();
        $job->setDatasheet($filePath);
        $fs->copy(
            $filePath,
            $this->datasheetsDir.'/'.$job->getInputDatasheet(), 
            true
        );
        $job->setConstraintMining($constraint);
        $job->setFrequency($frequency);

        $memory = memory_get_usage();
        $time = hrtime(true);
        $this->constraintMining->compute($job->getConstraintMining(), $job);
        $memory = intval(memory_get_usage())-intval($memory);
        $time = intval(hrtime(true))-intval($time);

        $output->writeln("<bg=green> ✔ OK</> ".$filePath);
        $output->writeln(" * constraint: ".$constraint);
        $output->writeln(" * frequency: ".$frequency);
        
        $output->writeln(" * memory usage: ".$this->convertBytes2($memory, 'K')." KB");
        $output->writeln(" * time: ".$time/pow(10,9). " seconds");
        $output->writeln(" * output: ./public/uploads/datasheets/".$job->getOutputDatasheet());    
    }

    // https://isabelcastillo.com/php-convert-bytes
    function convertBytes2($bytes, $to, $decimal_places = 1) {
        $formulas = array(
            'K' => number_format($bytes / 1024, $decimal_places),
            'M' => number_format($bytes / 1048576, $decimal_places),
            'G' => number_format($bytes / 1073741824, $decimal_places)
        );
        return isset($formulas[$to]) ? $formulas[$to] : 0;
    }

}
