<?php
// src/Command/ImportLdapUserCommand.php
namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use Doctrine\ORM\EntityManagerInterface;



class ImportCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:import';
    protected $testServer = "mobipaleo.test.local.isima.fr";
    protected $prodServer = "mobipaleo.cri.local.isima.fr";
    protected $remoteSshUserKeyPath = "~/.ssh/stack/limosadm";
    protected $remoteDatasheetsDirPath = "/var/www/mobipaleo/current/public/uploads/datasheets";
    protected $remoteSshUser = "limosadm";
    protected $localDbUser = "admin";

    private $em;
    private $params;

    public function __construct(
        EntityManagerInterface $entityManager, 
        ParameterBagInterface $params
    )
    {
        $this->em = $entityManager;
        $this->params = $params;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addOption(
                'test',
                't',
                InputOption::VALUE_NONE
            )
            ->setDescription('Import app from prod or test.')
            ->setHelp('This command import app from prod or test ...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<fg=cyan>import schema</>");
        
        $database = $this->em->getConnection()->getDatabase();
        
        if($input->getOption('test'))
            $server = $this->testServer;
        else
            $server = $this->prodServer;
        
        # import csv
        shell_exec('rm -rf '.$this->params->get('datasheets_dir').'/*.csv');        
        shell_exec('scp  -i '.$this->remoteSshUserKeyPath.' '.$this->remoteSshUser.'@'.$server.':'.$this->remoteDatasheetsDirPath.'/*.csv '.$this->params->get('datasheets_dir').'/');        
        # import schema
        shell_exec('ssh -i '.$this->remoteSshUserKeyPath.' '.$this->remoteSshUser.'@'.$server.' "mysqldump '.$database.'" > /tmp/'.$database.'.sql');
        $dropCmd = $this->getApplication()->find('doctrine:database:drop');
        $dropCmd->run(new ArrayInput([ '--force'  => true ]), $output);
        $createCmd = $this->getApplication()->find('doctrine:database:create');
        $createCmd->run(new ArrayInput([]), $output);
        $createCmd = $this->getApplication()->find('enqueue:setup-broker');
        $createCmd->run(new ArrayInput([]), $output);

        shell_exec('mysql -u '.$this->localDbUser.' '.$database.' < /tmp/'.$database.'.sql');
        shell_exec('rm /tmp/'.$database.'.sql');

        $output->writeln("<fg=cyan>app imported</>");

    }
}