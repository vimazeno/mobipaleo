<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class DocsController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        return $this->render('docs/home.html.twig');
    }

    /**
     * @Route("/tutorial", name="tutorial")
     */
    public function tutorial()
    {
        return $this->render('docs/tutorial.html.twig');
    }

    /**
     * @Route("/members", name="members")
     */
    public function members()
    {
        return $this->render('docs/members.html.twig');
    }

    /**
     * @Route("/publications", name="publications")
     */
    public function publications()
    {
        return $this->render('docs/publications.html.twig');
    }

    /**
     * @Route("/description", name="description")
     */
    public function description()
    {
        return $this->render('docs/description.html.twig');
    }

    /**
     * @Route("/references", name="references")
     */
    public function references()
    {
        return $this->render('docs/references.html.twig');
    }

    /**
     * @Route("/privacy", name="privacy")
     */
    public function privacy()
    {
        return $this->render('docs/privacy.html.twig');
    }

}
