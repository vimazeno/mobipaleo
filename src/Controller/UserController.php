<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;
use App\Form\ChangePasswordFormType;
use Knp\Component\Pager\PaginatorInterface;

class UserController extends AbstractController
{
    /**
     * @Route(
     *  "/profile", 
     *  name="profile"
     * )
     * @IsGranted("ROLE_USER")
     */
    public function profile(
        Request $request, 
        UserPasswordEncoderInterface $passwordEncoder
    ): Response
    {
        $form = $this->createForm(
            ChangePasswordFormType::class, 
            $this->getUser()
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getUser()->setPassword(
                $passwordEncoder->encodePassword(
                    $this->getUser(),
                    $form->get('password')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($this->getUser());
            $entityManager->flush();

            $this->addFlash('succes', 'Password updated');
            return $this->redirectToRoute('job');
        }

        return $this->render('user/profile.html.twig', [
            'changePasswordForm' => $form->createView(),
        ]);
    }

    /**
     * @Route(
     *  "/users", 
     *  name="users"
     * )
     * @IsGranted("ROLE_ADMIN")
     */
    public function users(
        Request $request,
        PaginatorInterface $paginator
    ): Response
    {
        $usersQuery = $this->getDoctrine()
        ->getRepository(User::class)
        ->createQueryBuilder('u')
        ->getQuery();
        
        $users = $paginator->paginate(
            $usersQuery,
            $request->query->getInt('page', 1)
        );

        return $this->render('user/list.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route(
     *  "/user/enable/{id<\d+>}/{enable<[0,1]>}", 
     *  name="user_enable"
     * )
     * @IsGranted("ROLE_ADMIN")
     */
    public function userEnable(
        $id,
        $enable,
        Request $request
    ): Response
    {
        $user = $this->getDoctrine()
        ->getRepository(User::class)
        ->findOneBy([
            'id' => $id
        ]);

        $user->setEnable($enable);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();


        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
        
    }

    /**
     * @Route(
     *  "/user/admin/{id<\d+>}/{admin<[0,1]>}", 
     *  name="user_admin"
     * )
     * @IsGranted("ROLE_ADMIN")
     */
    public function userAdmin(
        $id,
        $admin,
        Request $request
    ): Response
    {
        $user = $this->getDoctrine()
        ->getRepository(User::class)
        ->findOneBy([
            'id' => $id
        ]);

        $user->setRoles(['ROLE_USER']);

        if($admin)
        {
            $user->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
        
    }
}
