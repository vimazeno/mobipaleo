<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use App\Entity\User;
use App\Form\LoginFormType;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function login(
        AuthenticationUtils $authenticationUtils
    ): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->createForm(LoginFormType::class, new User());
        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
            'loginForm'     => $form->createView()
        ]);
    }

    /**
    * @Route("/forgotten-password", name="forgotten_password")
    */
    public function forgottenPassword(
        Request $request,
        UserPasswordEncoderInterface $encoder,
        \Swift_Mailer $mailer,
        CsrfTokenManagerInterface $tokenGenerator
    ): Response
   {
        if ($request->isMethod('POST')) {
    
            $email = $request->request->get('email');

            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)
                ->findOneByEmail($email);
            
            if ($user !== null) {
            
                $token = $tokenGenerator->getToken('reset_password');
                $token = $token->getValue();

                try{
                    $user->setResetToken($token);
                    $entityManager->flush();
                } catch (\Exception $e) {
                    $this->addFlash('danger', $e->getMessage());
                    return $this->redirectToRoute('home');
                }

                $url = $this->generateUrl(
                    'reset_password', 
                    array('token' => $token),
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $message = (new \Swift_Message('Forgot Password'))
                    ->setFrom($this->getParameter('email_from'))
                    ->setTo($user->getEmail())
                    ->setBody(
                        'Here is your reset password link : ' .
                        '<a href="'.$url.'">'.$url.'</a>',
                        'text/html'
                    );

                $mailer->send($message);
            
            }

            $this->addFlash('warning', 'an email was sent to '.$email);

            return $this->redirectToRoute('job');
        }
        
        return $this->render('security/forgotten_password.html.twig');
   }

    /**
     * @Route("/reset-password/{token}", name="reset_password")
     */
    public function resetPassword(
        Request $request, 
        string $token, 
        UserPasswordEncoderInterface $passwordEncoder
    ): Response
    {

        if ($request->isMethod('POST')) 
        {
            $entityManager = $this->getDoctrine()->getManager();

            $user = $entityManager
                ->getRepository(User::class)
                ->findOneByResetToken($token);

            if ($user === null) {
                $this->addFlash('danger', 'Token Unknown token');
                return $this->redirectToRoute('home');
            }

            $user->setResetToken(null);
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user, 
                    $request->request->get('password')
                )
            );
            $entityManager->flush();

            $this->addFlash('success', 'Password updated');

            return $this->redirectToRoute('job');
        }
        
        return $this->render(
            'security/reset_password.html.twig', 
            [
                'token' => $token
            ]
        );

    }

}