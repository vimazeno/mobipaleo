<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Alert;

class AlertController extends AbstractController
{
    public function list()
    {
        $criteria = ['user' => $this->getUser()];

        $orders = [
            'startDate' => 'DESC',
            'endDate' => 'DESC'
        ];

        $alerts = $this->getDoctrine()
            ->getRepository(Alert::class)
            ->findByUser($criteria, $orders);

        return $this->render('alert/list.html.twig', [
            'alerts' => $alerts,
        ]);
    }

}
