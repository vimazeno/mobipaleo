<?php

namespace App\Controller;

use App\Entity\Job;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class JobDatasheetController extends AbstractController
{

    /**
     * @Route(
     *  "/job/input-datasheet/{id<\d+>}",
     *  name="job_datasheet_input",
     * )
     * @IsGranted("ROLE_USER")
     */
    public function inputDatasheet($id, AuthorizationCheckerInterface $authChecker)
    {
        // display user job
        if(false === $authChecker->isGranted('ROLE_ADMIN'))
        {
            $job_criteria = [
                'id' => $id,
                'user' => $this->getUser(),
            ];

        }
        // display any user job for admin
        else
        {
            $job_criteria = [
                'id' => $id,
            ];
   
        }

        $job = $this->getDoctrine()
        ->getRepository(Job::class)
        ->findOneBy($job_criteria);

        $datasheetPath = $this->getParameter('datasheets_dir').'/'.$job->getInputDatasheet();
        $response = new BinaryFileResponse($datasheetPath);        
        $response->headers->set('Content-Type', 'text/csv');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $job->getTitle().'.in.csv'
        );

        return $response;
    }

    /**
     * @Route(
     *  "/job/output-datasheet/{id<\d+>}",
     *  name="job_datasheet_output",
     * )
     * @IsGranted("ROLE_USER")
     */
    public function outputDatasheet(Request $request, AuthorizationCheckerInterface $authChecker, $id)
    {
         // display user job
         if(false === $authChecker->isGranted('ROLE_ADMIN'))
         {
             $job_criteria = [
                 'id' => $id,
                 'user' => $this->getUser(),
             ];
 
         }
         // display any user job for admin
         else
         {
             $job_criteria = [
                 'id' => $id,
             ];
    
         }

        $job = $this->getDoctrine()
        ->getRepository(Job::class)
        ->findOneBy($job_criteria);

        $excludedColumns = array_diff(
            $request->request->has('columns')?$request->request->get('columns'):[],
            $request->request->has('selected_columns')?$request->request->get('selected_columns'):[]
        );

        $datasheetPath = '/tmp/'.md5(uniqid());
        $fp = fopen($datasheetPath, 'w');

        $results = $job->getResults(
            $this->getParameter('datasheets_dir'),
            $excludedColumns
        );

        foreach ($results as $line) {
            $columns = [];
            foreach($line['columns'] as $column)
            {
                preg_match_all('/([\d]+)([\+|\-])/', $column, $record);
                $columns[] = $job->getColumns()[(int)$record[1][0]-1].$record[2][0];                
            }
            $l = [implode(' ', $columns), $line['frequency']];
            if(array_key_exists('tids', $line))
            {
                $l[] = implode(' ', $line['tids']);
            }
            fputcsv(
                $fp, $l
            );    
        }

        $response = new BinaryFileResponse($datasheetPath);
        $response->headers->set('Content-Type', 'text/csv');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $job->getTitle().'.out.csv'
        )->deleteFileAfterSend(true);

        return $response;
    }

}
