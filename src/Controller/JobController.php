<?php

namespace App\Controller;

use App\Entity\Job;
use App\Entity\Alert;
use App\Entity\User;
use App\Form\JobType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

use Enqueue\Client\ProducerInterface;

use App\Services\ConstraintMining;


class JobController extends AbstractController
{
    /**
     * @Route("/job/{id<\d+>}", name="job", defaults={"id"=0})
     * @IsGranted("ROLE_USER")
     */
    public function list(
        Request $request, 
        ProducerInterface $producer, 
        $id, 
        AuthorizationCheckerInterface $authChecker,
        ParameterBagInterface $params)
    {
        // job submision
        $job = new Job();
        $form = $this->createForm(JobType::class, $job, [
            'csrf_protection' => false,
            'attr' => ['id' => 'job-form']
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $job = $form->getData();
            $datasheet = $form->get('datasheet')->getData();
            $datasheetName = md5(uniqid());
            try {
                $datasheet->move(
                    $this->getParameter('datasheets_dir'),
                    $datasheetName.'.in.csv'
                );
                
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }
            $job->setDatasheet($datasheetName);
            $job->setUser($this->getUser());
            $job->setStartDate(new \DateTime('now'));        

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($job);
            $entityManager->flush();
            
            $producer->sendEvent(
                'JobTopic', 
                $job->getId().';'.$this->getUser()->getEmail()
            );
            
            return $this->render('job/inc/card.html.twig', [
                'job' => $job,
                'constraintMiningChoices' => array_flip(ConstraintMining::$constraintMiningChoices),
                'maxLinesToShow' => $params->get('max_lines_to_show'),
            ]);
        }

        // filter user jobs
        if(false === $authChecker->isGranted('ROLE_ADMIN'))
        {
            $criteria = ['user' => $this->getUser()];
        }
        // all jobs for admin
        else
        {
            if($id)
            {
                $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->find($id);
                dump($user);
                $criteria = ['user' => $user];
            }
            else
            {
                $criteria = [];
            }
        }

        $orders = [
            'startDate' => 'DESC',
            'endDate' => 'DESC'
        ];

        $jobs = $this->getDoctrine()
        ->getRepository(Job::class)
        ->findBy($criteria, $orders);

        return $this->render('job/list.html.twig', [
            'form' => $form->createView(),
            'jobs' => $jobs,
            'constraintMiningChoices' => array_flip(ConstraintMining::$constraintMiningChoices),
            'maxLinesToShow' => $params->get('max_lines_to_show'),
        ]);
    }

    /**
     * @Route(
     *  "/job/show/{id<\d+>}",
     *  options = { "expose" = true },
     *  name="job_show",
     * )
     * @IsGranted("ROLE_USER")
     */
    public function show($id, AuthorizationCheckerInterface $authChecker, ParameterBagInterface $params)
    {
        // display user job
        if(false === $authChecker->isGranted('ROLE_ADMIN'))
        {
            $job_criteria = [
                'id' => $id,
                'user' => $this->getUser(),
            ];
        }
        // display any user job for admin
        else
        {
            $job_criteria = [
                'id' => $id,
            ];
        }

        $job = $this->getDoctrine()
        ->getRepository(Job::class)
        ->findOneBy($job_criteria);

        // mark as read if first show for user
        if(false === $authChecker->isGranted('ROLE_ADMIN'))
        {
            $job->getAlert()->setReaded(True);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($job);
            $entityManager->flush();
        }

        return $this->render('job/show.html.twig', [
            'job' => $job,
            'constraintMiningChoices' => array_flip(ConstraintMining::$constraintMiningChoices),
            'maxLinesToShow' => $params->get('max_lines_to_show'),
        ]);
    }

    /**
     * @Route(
     *  "/job/delete/{id<\d+>}",
     *  name="job_delete",
     * )
     * @IsGranted("ROLE_USER")
     */
    public function delete($id, AuthorizationCheckerInterface $authChecker)
    {

        // display user job
        if(false === $authChecker->isGranted('ROLE_ADMIN'))
        {
            $job_criteria = [
                'id' => $id,
                'user' => $this->getUser(),
            ];
        }
        // display any user job for admin
        else
        {
            $job_criteria = [
                'id' => $id,
            ];
        }

        $job = $this->getDoctrine()
        ->getRepository(Job::class)
        ->findOneBy($job_criteria);

        $job->deleteDatasheet($this->getParameter('datasheets_dir'));
        
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($job);
        $entityManager->flush();

        return $this->redirectToRoute('home');
    }

}
