<?php

namespace App\Processor;

use App\Entity\Job;
use App\Entity\Alert;
use App\Services\ConstraintMining;

use Interop\Queue\Message;
use Interop\Queue\Context;
use Interop\Queue\Processor;
use Enqueue\Client\TopicSubscriberInterface;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Mercure\PublisherInterface;

class JobProcessor implements Processor, TopicSubscriberInterface
{
    
    private $logger;
    private $publisher;
    private $em;
    private $constraintMining;

    public function __construct(
        LoggerInterface $logger, 
        PublisherInterface $publisher,
        EntityManagerInterface $entityManager, 
        ConstraintMining $constraintMining,
        ParameterBagInterface $params
    ) 
    {
        $this->logger = $logger;
        $this->publisher = $publisher;
        $this->em = $entityManager;
        $this->constraintMining = $constraintMining;
        $this->baseMercureUserTopicIri = $params->get('base_mercure_user_topic_iri');        
    }

    public function process(Message $message, Context $session)
    {
        list($jobId, $userEmail) = explode(';', $message->getBody());

        $job = $this->em
            ->getRepository(Job::class)
            ->find($jobId);

        $this->logger->info('process : '.$job->getInputDatasheet());
        
        $finalizedJob = $this->constraintMining->compute($job->getConstraintMining(), $job);
            
        $this->em->persist($finalizedJob);
        $this->em->flush();

        $alerts = $this->em
            ->getRepository(Alert::class)
            ->findByJob($job);

        if(count($alerts) == 0) {

            $alert = new Alert();
            $alert->setJob($job);
            $alert->setReaded(False);

            $this->em->persist($alert);
            $this->em->flush();

        }
        
        $update = new Update(
            $this->baseMercureUserTopicIri.'/'.$userEmail, 
            json_encode($job)
        );
        
        $publisher = $this->publisher;
        $publisher($update);

        $this->logger->info('processed : '.$job->getInputDatasheet());

        return self::ACK;
        // return self::REJECT; // when the message is broken
        // return self::REQUEUE; // the message is fine but you want to postpone processing
    }

    public static function getSubscribedTopics()
    {
        return ['JobTopic'];
    }
}