<?php

namespace App\DataFixtures;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;


class UserFixtures extends Fixture
{
    private $passwordEncoder;
    public const ADMIN_REFERENCE = 'admin';
    public const USER1_REFERENCE = 'user1';
    public const USER2_REFERENCE = 'user2';
    public $users = [
        [
            'email' => 'admin@mobi.paleo', 
            'password' => 'pipopipo', 
            'reference' => self::ADMIN_REFERENCE,
            'roles' => ['ROLE_USER', 'ROLE_ADMIN'],
        ],
        [
            'email' => 'user1@mobi.paleo', 
            'password' => 'pipopipo', 
            'reference' => self::USER1_REFERENCE,
            'roles' => ['ROLE_USER'],
        ],
        [
            'email' => 'user2@mobi.paleo', 
            'password' => 'pipopipo', 
            'reference' => self::USER2_REFERENCE,
            'roles' => ['ROLE_USER'],
        ],
    ];

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        foreach($this->users as $u)
        {
            $user = new User();
            $user->setEmail($u['email']);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $u['password']
            ));
            $user->setRoles($u['roles']);
            $manager->persist($user);
            $manager->flush();
            $this->addReference($u['reference'], $user);
        }
        
    }
}
