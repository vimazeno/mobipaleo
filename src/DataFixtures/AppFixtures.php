<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use App\Entity\Alert;
use App\Entity\Job;
use App\Services\ConstraintMining;


class AppFixtures extends Fixture implements DependentFixtureInterface
{
    private $datasheetsDir;
    private $constraintMining;
    private $jobFiles = [
        [ 
            'file' => 'distant-example1-input',
            'frequency' => 12,
            'constraint_mininig' => ConstraintMining::CONSTRAINT_MINING_DISTANT,
            'readed' => False,
            'user' => UserFixtures::USER1_REFERENCE
        ],
        [ 
            'file' => 'consecutive-example1-input',
            'frequency' => 12,
            'constraint_mininig' => ConstraintMining::CONSTRAINT_MINING_CONSECUTIVE,
            'readed' => False,
            'user' => UserFixtures::USER1_REFERENCE
        ],
        [ 
            'file' => 'distant-example2-input',
            'frequency' => 12,
            'constraint_mininig' => ConstraintMining::CONSTRAINT_MINING_DISTANT,
            'readed' => False,
            'user' => UserFixtures::USER2_REFERENCE
        ],
        [ 
            'file' => 'consecutive-example2-input',
            'frequency' => 12,
            'constraint_mininig' => ConstraintMining::CONSTRAINT_MINING_CONSECUTIVE,
            'readed' => True,
            'user' => UserFixtures::USER2_REFERENCE
        ],

    ];

    public function __construct(
        ConstraintMining $constraintMining,
        ParameterBagInterface $params        
    ) 
    {
        $this->constraintMining = $constraintMining;
        $this->fixturesDatasheetsDir = $params->get('fixtures_datasheets_dir');
        $this->datasheetsDir = $params->get('datasheets_dir');
    }

    public function load(ObjectManager $manager)
    {
        $fs = new Filesystem();        
        
        foreach($this->jobFiles as $j) 
        {
            $job = new Job();
            $user = $this->getReference($j['user']);
            $job->setDatasheet($j['file'].'-'.$j['user']);
            $fs->copy(
                $this->fixturesDatasheetsDir.'/'.$j['file'].'.csv',
                $this->datasheetsDir.'/'.$job->getInputDatasheet(), 
                true
            );
            $job->setUser($user);
            $job->setTitle($j['file']);
            $job->setConstraintMining($j['constraint_mininig']);
            $job->setFrequency($j['frequency']);
            
            $finalizedJob = $this->constraintMining->compute($job->getConstraintMining(), $job);
            
            $manager->persist($finalizedJob);
            $manager->flush();
            
            $alert = new Alert();
            $alert->setJob($job);
            $alert->setReaded($j['readed']);
            $manager->persist($alert);

            $manager->flush();
        }
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}
