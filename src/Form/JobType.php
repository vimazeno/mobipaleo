<?php

namespace App\Form;
use App\Entity\Job;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Services\ConstraintMining;

class JobType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('title', TextType::class, [
            'required'   => true
        ])
        ->add('constraint_mining', ChoiceType::class, [
            'required'   => true,
            'choices'  => array_merge(
                ['Choose a temporal constraint between objects ...' => 0],
                ConstraintMining::$constraintMiningChoices
            )
        ])
        ->add('frequency', IntegerType::class, [
            'required'   => true
        ])
        ->add('datasheet', FileType::class, [
            'required'   => true,
            'label' => 'Datasheet (.dat)', // see in templates/job/list.html.twig for link
        ]) 
        ->add('save', SubmitType::class, [
            'label' => 'Submit Job', 
            'attr' => [
                'class' => 'btn btn-primary float-right'
            ]
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Job::class,
        ]);
    }
}