<?php

namespace App\Services;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ConstraintMiningConsecutiveWithTids
{
    private $datasheetsDir;
    private $binaryPath;
    private $timeout;
    private $idleTimeOut;

    public function __construct(ParameterBagInterface $params) 
    {
        $this->datasheetsDir = $params->get('datasheets_dir');
        $this->path = $params->get('constraint_mining_consecutive_with_tids_path');
        $this->cmd = $params->get('constraint_mining_consecutive_with_tids_cmd');
        $this->rScript = $params->get('constraint_mining_consecutive_with_tids_r_script');
        $this->timeout = $params->get('constraint_mining_consecutive_with_tids_timeout');
        $this->idleTimeOut = $params->get('constraint_mining_consecutive_with_tids_idle_timeout');
    }

    public function compute($job) 
    {
        $inputDatasheetPath = $this->datasheetsDir.'/'.$job->getInputDatasheet();
        $outputDatasheetPath = $this->datasheetsDir.'/'.$job->getOutputDatasheet();

        $line = fgets(fopen($inputDatasheetPath, 'r'));

        $job->setColumns(preg_split('/\s+/', trim($line)));

        $job->setStartDate(new \DateTime('now'));        

        $cmd = [
            $this->cmd, 
            $this->rScript, 
            $inputDatasheetPath,
            $job->getFrequency()/100,
            $outputDatasheetPath
        ];
        $process = new Process($cmd);
        $process->setWorkingDirectory($this->path);
        $process->setTimeout($this->timeout);
        $process->setIdleTimeout($this->idleTimeOut);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        // structure results as array
        $results = Array();
        $data = file_get_contents($outputDatasheetPath);
        $lines = explode("\n", $data);
        $lines = array_slice($lines, 1, -4);
        $job->setNbLines(count($lines));        
        
        foreach($lines as $k => $line)
        {
            $results[$k] = Array();
            preg_match_all('/([\w\.]+=[\+|\-])/', $line, $columns);
            $results[$k]['columns'] = array_map(
                function ($c) { 
                    $column = str_replace("X", "", $c);
                    $column = str_replace("=", "", $column);
                    return $column; 
                }, 
                $columns[0]
            );
            
            $columns = preg_split('/\s+/', trim($line));
            $frequency = array_slice($columns, -2, -1)[0];
            $tids = array_slice($columns, -3, -2)[0];
            $tids = trim(trim(trim($tids, '"'),'{'),'}');
            $results[$k]['frequency'] = intval(round($frequency*100));
            $results[$k]['tids'] = explode(';', $tids);
        }
        
        // store serialized array
        file_put_contents($outputDatasheetPath, serialize($results));

        $job->setEndDate(new \DateTime('now'));

        return $job;
    }
    
}
