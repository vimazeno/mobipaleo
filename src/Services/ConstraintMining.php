<?php

namespace App\Services;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use App\Services\ConstraintMiningConsecutive;
use App\Services\ConstraintMiningConsecutiveWithTids;
use App\Services\ConstraintMiningDistant;

class ConstraintMining
{
    public const CONSTRAINT_MINING_CONSECUTIVE = 'consecutive';
    public const CONSTRAINT_MINING_CONSECUTIVE_WITH_TIDS = 'consecutive_with_tids';
    public const CONSTRAINT_MINING_DISTANT = 'distant';
    public const CONSTRAINT_MINING_CONSECUTIVE_LABEL = 'Temporal constraint between consecutive objects';
    public const CONSTRAINT_MINING_CONSECUTIVE_WITH_TIDS_LABEL = 'Temporal constraint between consecutive objects with tids';
    public const CONSTRAINT_MINING_DISTANT_LABEL = 'Temporal constraint between distant objects';
    
    public static $constraintMiningChoices = [
        self::CONSTRAINT_MINING_CONSECUTIVE_LABEL => self::CONSTRAINT_MINING_CONSECUTIVE,
        self::CONSTRAINT_MINING_CONSECUTIVE_WITH_TIDS_LABEL => self::CONSTRAINT_MINING_CONSECUTIVE_WITH_TIDS,
        self::CONSTRAINT_MINING_DISTANT_LABEL => self::CONSTRAINT_MINING_DISTANT,
    ];

    private $constraintMinings = [];

    public function __construct(
        ConstraintMIningConsecutive $constraintMiningConsecutive,
        ConstraintMIningConsecutiveWithTids $constraintMiningConsecutiveWithTids,
        ConstraintMIningDistant $constraintMiningDistant
    ) 
    {
        $this->constraintMinings[self::CONSTRAINT_MINING_CONSECUTIVE] = $constraintMiningConsecutive;
        $this->constraintMinings[self::CONSTRAINT_MINING_CONSECUTIVE_WITH_TIDS] = $constraintMiningConsecutiveWithTids;
        $this->constraintMinings[self::CONSTRAINT_MINING_DISTANT] = $constraintMiningDistant;   
    }

    public function compute($constraint_mining, $job) 
    {
        return $this->constraintMinings[$constraint_mining]->compute($job);
    }
    
}
