<?php

namespace App\Services;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ConstraintMiningConsecutive
{
    private $datasheetsDir;
    private $binaryPath;
    private $timeout;
    private $idleTimeOut;

    public function __construct(ParameterBagInterface $params) 
    {
        $this->datasheetsDir = $params->get('datasheets_dir');
        $this->binaryPath = $params->get('constraint_mining_consecutive_path');
        $this->timeout = $params->get('constraint_mining_consecutive_timeout');
        $this->idleTimeOut = $params->get('constraint_mining_consecutive_idle_timeout');
    }

    public function compute($job) 
    {
        $inputDatasheetPath = $this->datasheetsDir.'/'.$job->getInputDatasheet();
        $outputDatasheetPath = $this->datasheetsDir.'/'.$job->getOutputDatasheet();

        $line = fgets(fopen($inputDatasheetPath, 'r'));

        $job->setColumns(preg_split('/\s+/', trim($line)));

        $job->setStartDate(new \DateTime('now'));        

        $cmd = [
            $this->binaryPath, 
            $job->getFrequency()/100,
            1,
            $inputDatasheetPath,
            $outputDatasheetPath
        ];

        $process = new Process($cmd);
        $process->setTimeout($this->timeout);
        $process->setIdleTimeout($this->idleTimeOut);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $patterns = array_map(
            function ($c) { 
                $regexp = preg_replace('/(.+)/','/${1}/', $c);
                $regexp = str_replace("-", ".", $regexp);
                return $regexp;
            }, 
            $job->getColumns()
        );
        
        $replacements = array_map(
            function ($c) { 
                return $c+1;
            }, 
            array_keys($job->getColumns())
        );

        // structure results as array
        $results = Array();
        $data = file_get_contents($outputDatasheetPath);
        $lines = explode("\n", $data);
        $lines = array_slice($lines, 1, -1);
        $job->setNbLines(count($lines));        

        foreach($lines as $k => $line)
        {
            $results[$k] = Array();
            preg_match_all('/([\w\.]+=[\+|\-])/', $line, $columns);
            $results[$k]['columns'] = array_map(
                function ($c) use ($patterns, $replacements) { 
                    $regexp = preg_replace($patterns, $replacements, $c);
                    $regexp = str_replace("=", "", $regexp);
                    return $regexp; 
                }, 
                $columns[0]
            );
            
            $frequency = preg_split('/\s+/', trim($line));
            $frequency = array_slice($frequency, -2, -1)[0];
            $results[$k]['frequency'] = intval(round($frequency*100));
        }
        
        // store serialized array
        file_put_contents($outputDatasheetPath, serialize($results));

        $job->setEndDate(new \DateTime('now'));

        return $job;
    }
    
}
