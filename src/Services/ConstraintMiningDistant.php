<?php

namespace App\Services;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ConstraintMiningDistant
{
    private $datasheetsDir;
    private $binaryPath;
    private $timeout;
    private $idleTimeOut;

    public function __construct(ParameterBagInterface $params) 
    {
        $this->datasheetsDir = $params->get('datasheets_dir');
        $this->binaryPath = $params->get('constraint_mining_distant_path');
        $this->timeout = $params->get('constraint_mining_distant_timeout');
        $this->idleTimeOut = $params->get('constraint_mining_distant_idle_timeout');
    }

    public function compute($job) 
    {
        $inputDatasheetPath = $this->datasheetsDir.'/'.$job->getInputDatasheet();
        $outputDatasheetPath = $this->datasheetsDir.'/'.$job->getOutputDatasheet();

        $line = fgets(fopen($inputDatasheetPath, 'r'));
        
        $job->setColumns(preg_split('/\s+/', trim($line)));        

        $job->setStartDate(new \DateTime('now'));        

        $cmd = [
            $this->binaryPath, 
            $inputDatasheetPath, 
            $job->getFrequency() 
        ];

        $process = new Process($cmd);
        $process->setTimeout($this->timeout);
        $process->setIdleTimeout($this->idleTimeOut);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        // structure results as array
        $results = Array();
        $data = $process->getOutput();
        $lines = explode("\n", $data);
        $lines = array_slice($lines, 3, -2);
        $job->setNbLines(count($lines));        
        
        foreach($lines as $k => $line)
        {
            $results[$k] = Array();
            preg_match_all('/(\d+[\+\-]+)\s+/', $line, $columns);
            $results[$k]['columns'] = $columns[1];
            preg_match_all('/\((\d+)\)$/', $line, $frequency);            
            $results[$k]['frequency'] = $frequency[1][0];
        }
        
        // store serialized array
        file_put_contents($outputDatasheetPath, serialize($results));

        $job->setEndDate(new \DateTime('now'));

        return $job;
    }
    
}
