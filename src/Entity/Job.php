<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JobRepository")
 */
class Job implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(allowNull = true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(allowNull = false)
     */
    private $constraint_mining;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please, upload data in the .dat format")
     * @App\Validator\Constraints\DatFileFormat
     * @Assert\File(
     *  mimeTypes={ "text/plain", "text/csv" }, 
     *  mimeTypesMessage="!The mime type of the file is invalid ({{ type }}). Allowed mime types are {{ types }}."
     * )
     */
    private $datasheet;

    /**
     * @ORM\Column(type="integer")
     */
    private $frequency;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="jobs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="array")
     */
    private $columns = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbLines;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Alert", mappedBy="job", cascade={"persist", "remove"})
     */
    private $alert;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getConstraintMining(): ?string
    {
        return $this->constraint_mining;
    }

    public function setConstraintMining(string $constraint_mining): self
    {
        $this->constraint_mining = $constraint_mining;

        return $this;
    }

    public function getDatasheet(): ?string
    {
        return $this->datasheet;
    }

    public function setDatasheet(string $datasheet): self
    {
        $this->datasheet = $datasheet;

        return $this;
    }

    public function deleteDatasheet(string $datasheetsDir): self
    {
        $fileSystem = new Filesystem();
        $InputDatasheetPath = $datasheetsDir.'/'.$this->getInputDatasheet();
        $OutputDatasheetPath = $datasheetsDir.'/'.$this->getOutputDatasheet();
        $fileSystem->remove([
            $InputDatasheetPath, 
            $OutputDatasheetPath
        ]);

        return $this;
    }

    public function getInputDatasheet(): ?string
    {
        return pathinfo($this->datasheet, PATHINFO_FILENAME).'.in.csv';
    }

    public function getOutputDatasheet(): ?string
    {
        return pathinfo($this->datasheet, PATHINFO_FILENAME).'.out.csv';
    }

    public function getFrequency(): ?int
    {
        return $this->frequency;
    }

    public function setFrequency(int $frequency): self
    {
        $this->frequency = $frequency;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate = null): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getColumns(): ?array
    {
        return $this->columns;
    }

    public function getColumn($index): ?string
    {
        return $this->columns[$index-1];
        return $index-1;
    }

    public function setColumns(array $columns): self
    {
        $this->columns = $columns;

        return $this;
    }

    public function getNbLines(): ?int
    {
        return $this->nbLines;
    }

    public function setNbLines(int $nbLines): self
    {
        $this->nbLines = $nbLines;

        return $this;
    }

    public function getAlert(): ?Alert
    {
        return $this->alert;
    }

    public function setAlert(Alert $alert): self
    {
        $this->alert = $alert;

        // set the owning side of the relation if necessary
        if ($alert->getJob() !== $this) {
            $alert->setJob($this);
        }

        return $this;
    }

    public function getResults(string $datasheetsDir, $excludedColumns = []): ?array
    {
        $results = unserialize(
                file_get_contents(
                    $datasheetsDir.'/'.$this->getOutputDatasheet()
                )
            );                

        foreach ($results as $k => $line) {

            $present = False;

            foreach($excludedColumns as $excludedColumn)
            {
                if(
                    in_array($excludedColumn."+", $line['columns']) 
                    || in_array($excludedColumn."-", $line['columns']) 
                )
                {
                    $present = True;
                }
            }

            if($present)
            {
                unset($results[$k]);
            }
        }
    
        return $results;
    }

    public function jsonSerialize() : array
    {
        return [
            'id'                => $this->id,
            'columns'           => $this->columns,
            'nbLines'           => $this->nbLines,
            'title'             => $this->title,
            'datasheet'         => $this->datasheet,
            'frequency'         => $this->frequency,
            'constraint_mining' => $this->constraint_mining,
            'startDate'         => date_format($this->startDate, 'Y-m-d H:i:s'),
            'endDate'           => date_format($this->endDate, 'Y-m-d H:i:s'),
            'user'              => $this->user->getId(),
            'columns'           => $this->columns,
        ];
    }

}
