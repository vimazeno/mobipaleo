## run mobipaleo app for dev

provisionner

```
/etc/ssl/certs/star.dev.isima.fr.crt
/etc/ssl/private/star.dev.isima.fr.key
```

exécuter

```
bash bin/dev.sh
cd mercure && sudo bash ./mercure.sh
```

## notes

les certs du serveurs mercure doivent être valide et le cert doit présenter cert + root ca cert

## TODO

* configurer le  mail
* testing
* monitor memory usage https://askubuntu.com/questions/9642/how-can-i-monitor-the-memory-usage

