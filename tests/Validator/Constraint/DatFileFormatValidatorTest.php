<?php
namespace App\Tests\Validator\Constraint;
 
use App\Validator\Constraints\DatFileFormatValidator;
use PHPUnit\Framework\TestCase;
use App\Validator\Constraints\DatFileFormat;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;
 
class DatFileFormatValidatorTest extends TestCase
{
    private $datFileFormatValidator;
    private $executionContextMock;
    private $constraintViolationBuilderMock;
    private $constraintMock;
 
    protected function setUp()
    {
        $this->datFileFormatValidator = new datFileFormatValidator();

        $this->executionContextMock = $this->getMockBuilder(ExecutionContextInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->constraintViolationBuilderMock = $this->getMockBuilder(ConstraintViolationBuilderInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->constraintMock = $this->getMockBuilder(DatFileFormat::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testBlankFilePath()
    {   
        $this->constraintViolationBuilderMock
            ->expects($this->once())
            ->method('addViolation')
            ->willReturn(null);

        $this->executionContextMock
            ->expects($this->once())
            ->method('buildViolation')
            ->with(
                $this->constraintMock->message[
                    DatFileFormat::BLANK_FILE_PATH
                ]
            )
            ->willReturn($this->constraintViolationBuilderMock);
 
        $this->datFileFormatValidator->initialize($this->executionContextMock);
        $this->datFileFormatValidator->validate('', $this->constraintMock);
    
    }
    
    public function testColumnsConsistency()
    { 
        $this->constraintViolationBuilderMock
            ->expects($this->once())
            ->method('addViolation')
            ->willReturn(null);

        $this->executionContextMock
            ->expects($this->once())
            ->method('buildViolation')
            ->with(
                $this->constraintMock->message[
                    DatFileFormat::COLUMNS_CONSISTENCY
                ]
            )
            ->willReturn($this->constraintViolationBuilderMock);

        $this->datFileFormatValidator->initialize($this->executionContextMock);
        $this->datFileFormatValidator->validate(
            'datasheets/distant-example-with-inconsistent-number-of-columns-input.csv', 
            $this->constraintMock
        );

    }
    
    public function testHtmlTagsInColumns()
    { 
        $this->constraintViolationBuilderMock
            ->expects($this->once())
            ->method('addViolation')
            ->willReturn(null);

        $this->executionContextMock
            ->expects($this->once())
            ->method('buildViolation')
            ->with(
                $this->constraintMock->message[
                    DatFileFormat::HTML_TAGS_IN_COLUMNS
                ]
            )
            ->willReturn($this->constraintViolationBuilderMock);

        $this->datFileFormatValidator->initialize($this->executionContextMock);
        $this->datFileFormatValidator->validate(
            'datasheets/distant-example-with-html-in-columns-input.csv', 
            $this->constraintMock
        );

    }
    
    public function testNonNumericValues()
    { 
        $this->constraintViolationBuilderMock
            ->expects($this->once())
            ->method('addViolation')
            ->willReturn(null);

        $this->executionContextMock
            ->expects($this->once())
            ->method('buildViolation')
            ->with(
                $this->constraintMock->message[
                    DatFileFormat::NON_NUMERIC_VALUES
                ]
            )
            ->willReturn($this->constraintViolationBuilderMock);

        $this->datFileFormatValidator->initialize($this->executionContextMock);
        $this->datFileFormatValidator->validate(
            'datasheets/distant-example-with-non-numeric-in-values-input.csv', 
            $this->constraintMock
        );

    }
    
}