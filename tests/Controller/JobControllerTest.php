<?php
namespace App\Tests\Controller;

use Doctrine\DBAL\DriverManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\DataFixtures\UserFixtures;

class JobControllerTest extends WebTestCase
{
    private $fixtures;

    private function debug($var) {
        fwrite(STDERR, print_r($var, TRUE));
    }

    public function setUp() {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();      
        $this->fixtures = new UserFixtures(self::$container->get('security.password_encoder'));
        //$fixtures->load($entityManager);
    }
    
    public function testAnonHome()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testGoodLogin()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        
        $form = $crawler->selectButton('login')->form(array(
            '_username' => $this->fixtures->users[1]['email'],
            '_password' => $this->fixtures->users[1]['password'],
        ),'POST');
    
        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect());

        $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $crawler = $client->request('GET', '/');

        $this->assertTrue($crawler->filter('html:contains("Submit new job")')->count() > 0);
    }

    public function testBadLogin()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        
        $form = $crawler->selectButton('login')->form(array(
            '_username' => 'vmazenod@gmail.com',
            '_password' => 'xxxx',
        ),'POST');
    
        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect());

        $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $crawler = $client->request('GET', '/');

        $this->assertTrue($crawler->filter('html:contains("Submit new job")')->count() == 0);
    }

}